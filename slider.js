'use strict';

const cancel = document.createElement("button");
cancel.textContent = "Прекратить";
cancel.style.marginRight = "10px";
const reload = document.createElement("button");
reload.textContent = "Возобновить показ";

document.body.prepend(cancel);
cancel.after(reload);

let slideIndex = 1;

function showSlides(n) {
    let slides = document.getElementsByClassName("slide");
    if (n > slides.length) {
      slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
 
    for (let slide of slides) {
        slide.style.display = "none";
    }
    slides[slideIndex - 1].style.display = "flex";    
}

 let timer = setInterval(function(){
    slideIndex++;
    showSlides(slideIndex);
  },3000);

cancel.addEventListener('click',  ()=> {
    clearInterval(timer);
});
reload.addEventListener('click', () => {
 timer = setInterval(() => {
    slideIndex++;
    showSlides(slideIndex);
  }, 3000);
})










